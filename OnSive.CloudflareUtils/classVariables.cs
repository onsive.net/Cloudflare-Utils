﻿using CloudFlare.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnSive.CloudflareUtils
{
    public static class classVariables
    {
        /// <summary>
        /// The registry path to the application subkey
        /// </summary>
        public const string sSubKey = "SOFTWARE\\OnSive\\CloudflareUtilities";

        /// <summary>
        /// If Rollbar key was valid and the system is running
        /// </summary>
        public static bool bRollbarActive { get; set; }

        /// <summary>
        /// The cloudflare client for API requests
        /// </summary>
        public static CloudFlareClient Client { get; set; }
    }
}
