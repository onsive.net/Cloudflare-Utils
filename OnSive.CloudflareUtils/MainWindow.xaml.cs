﻿using CloudFlare.Client.Api.Result;
using CloudFlare.Client.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace OnSive.CloudflareUtils
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BlockingCollection<DnsRecord> BlockingCollection_DnsRecords = new BlockingCollection<DnsRecord>();
        bool bClearListBox = false;
        DateTime dtLastRefresh;

        public MainWindow()
        {
            try
            {

                InitializeComponent();

                Rectangle_Loading.Visibility = Visibility.Visible;
                ProgressBar_Loading.Visibility = Visibility.Visible;

                Task.Run(Render_DNS_Records);

                this.ContentRendered += MainWindow_ContentRendered;
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private async void Button_MassDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Rectangle_Loading.Visibility = Visibility.Visible;
                ProgressBar_Loading.Visibility = Visibility.Visible;
                ProgressBar_Loading.IsIndeterminate = false;

                List<DnsRecord> List_ToDelete = new List<DnsRecord>();

                foreach (UserControl_DNS oDNS in ListBox_DNS.Items)
                    if (oDNS.IsSelected)
                        List_ToDelete.Add((DnsRecord)oDNS.DataContext);

                ProgressBar_Loading.Maximum = List_ToDelete.Count;

                for (int i = 0; i < List_ToDelete.Count; i++)
                {
                    ProgressBar_Loading.Value = i;

                    CloudFlareResult<DnsRecord> CloudFlareResult_DnsRecord = await classVariables.Client.DeleteDnsRecordAsync(List_ToDelete[i].ZoneId, List_ToDelete[i].Id);

                    if (!CloudFlareResult_DnsRecord.Success)
                    {
                        classFunctions.OnError(new HttpRequestException(CloudFlareResult_DnsRecord.Messages.FirstOrDefault()?.Message));
                    }
                }

                ProgressBar_Loading.Value = 0;
                ProgressBar_Loading.IsIndeterminate = true;

                await Task.Run(Load_DNS);

                Rectangle_Loading.Visibility = Visibility.Collapsed;
                ProgressBar_Loading.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private async void Button_Refresh_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Rectangle_Loading.Visibility = Visibility.Visible;
                ProgressBar_Loading.Visibility = Visibility.Visible;

                await Task.Run(Load_DNS);

                Rectangle_Loading.Visibility = Visibility.Collapsed;
                ProgressBar_Loading.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (UserControl_DNS dns in ListBox_DNS.Items)
                {
                    dns.Select();
                }
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (UserControl_DNS dns in ListBox_DNS.Items)
                {
                    dns.UnSelect();
                }
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private void Load_DNS()
        {
            try
            {
                if (dtLastRefresh.AddMinutes(1) > DateTime.Now)
                {
                    Thread.Sleep(2000);
                    return;
                }

                bClearListBox = true;

                var CloudFlareResult_Zones = classVariables.Client.GetZonesAsync().Result;
                foreach (Zone oZone in CloudFlareResult_Zones.Result)
                {
                    var CloudFlareResult_DnsRecords = classVariables.Client.GetDnsRecordsAsync(oZone.Id).Result;
                    foreach (DnsRecord oDnsRecord in CloudFlareResult_DnsRecords.Result)
                    {
                        BlockingCollection_DnsRecords.Add(oDnsRecord);
                    }
                }

                dtLastRefresh = DateTime.Now;
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private async void MainWindow_ContentRendered(object sender, EventArgs e)
        {
            try
            {
                await Task.Run(Load_DNS);

                Rectangle_Loading.Visibility = Visibility.Collapsed;
                ProgressBar_Loading.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private void MenuItem_Exit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private void Render_DNS_Records()
        {
            try
            {
                foreach (DnsRecord oDnsRecord in BlockingCollection_DnsRecords.GetConsumingEnumerable())
                {
                    if (bClearListBox)
                    {
                        bClearListBox = false;
                        Application.Current.Dispatcher.BeginInvoke(
                            DispatcherPriority.Background,
                            new Action(() => ListBox_DNS.Items.Clear()));
                    }

                    Application.Current.Dispatcher.BeginInvoke(
                        DispatcherPriority.Background,
                        new Action(() => ListBox_DNS.Items.Add(new UserControl_DNS(oDnsRecord))));
                }
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                new wpfOptions(this).ShowDialog();
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }
    }
}
