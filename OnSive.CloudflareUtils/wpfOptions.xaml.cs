﻿using CloudFlare.Client;
using System;
using System.Linq;
using System.Windows;

namespace OnSive.CloudflareUtils
{
    /// <summary>
    /// Interaction logic for wpfOptions.xaml
    /// </summary>
    public partial class wpfOptions : Window
    {
        private bool bExitOnCancle;
        readonly bool bReInitClient;

        public wpfOptions(Window Owner, bool bExitOnCancle = false, bool bReInitClient = true)
        {
            try
            {
                this.bReInitClient = bReInitClient;
                this.bExitOnCancle = bExitOnCancle;
                if (Owner != null)
                {
                    this.Owner = Owner;
                }

                InitializeComponent();

                TextBox_Email.Text = classFunctions.Read_Registry("EMail");
                PasswordBox_ApiKey.Password = string.Empty.PadLeft(classFunctions.Read_Registry("Key").Length, '.');
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private void Button_Cancle_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (bExitOnCancle)
                    Environment.Exit(0);
                else
                    this.Close();
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private void Button_Save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                classFunctions.Write_Registry("EMail", TextBox_Email.Text);
                if(PasswordBox_ApiKey.Password.Count(x => x == '.') != PasswordBox_ApiKey.Password.Length)
                    classFunctions.Write_Registry("Key", PasswordBox_ApiKey.Password);

                if(bReInitClient)
                    classVariables.Client = new CloudFlareClient(classFunctions.Read_Registry("EMail"), classFunctions.Read_Registry("Key"));
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private void Button_SaveAndExit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button_Save_Click(sender, e);

                this.Close();
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }
    }
}
