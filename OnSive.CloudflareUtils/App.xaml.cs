﻿using CloudFlare.Client;
using Rollbar;
using System;
using System.Linq;
using System.Security.Authentication;
using System.Windows;

namespace OnSive.CloudflareUtils
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            string sRollbarKey = Environment.GetEnvironmentVariable("CloudflareUtitlities.RollbarConfig", EnvironmentVariableTarget.User);

            if (string.IsNullOrEmpty(sRollbarKey))
            {
                classVariables.bRollbarActive = false;
            }
            else
            {
                classVariables.bRollbarActive = true;

                var RollbarConfig_Tmp = new RollbarConfig(sRollbarKey)
                {
                    Person = new Rollbar.DTOs.Person()
                    {
                        UserName = Environment.MachineName
                    }
                };

                try
                {
                    RollbarLocator.RollbarInstance.Configure(RollbarConfig_Tmp);
                }
                catch (RollbarException)
                {
                    classVariables.bRollbarActive = false;
                }
            }

            try
            {
                string sEmail = "";
                string sAPIKey = "";

                do
                {
                    sEmail = classFunctions.Read_Registry("EMail");
                    sAPIKey = classFunctions.Read_Registry("Key");

                    if (string.IsNullOrEmpty(sEmail) && string.IsNullOrEmpty(sAPIKey))
                        new wpfOptions(null, true, false).ShowDialog();
                    else
                        break;

                } while (true);

                classVariables.Client = new CloudFlareClient(sEmail, sAPIKey);

                this.MainWindow = new MainWindow();
                this.MainWindow.ShowDialog();
            }
            catch (AuthenticationException)
            {
                MessageBox.Show("Your credentials for cloudflare are wrong!", "OnSive | Cloudflare Utils", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }

            Environment.Exit(0);
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            classVariables.Client.Dispose();
            // RollbarLocator.RollbarInstance.Dispose(); <-- Throws error??
        }
    }
}
