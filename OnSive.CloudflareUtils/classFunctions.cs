﻿using CloudFlare.Client;
using Microsoft.Win32;
using Rollbar;
using System;
using System.Windows;

namespace OnSive.CloudflareUtils
{
    public static class classFunctions
    {
        public static void OnError(Exception ex)
        {
#if DEBUG
            MessageBox.Show(ex.Message + "\n\n" + ex.StackTrace, "OnSive | Cloudflare Utils | Developer", MessageBoxButton.OK, MessageBoxImage.Error);
#else
            MessageBox.Show("An error occured.\n\nPlease try again later.", "OnSive | Cloudflare Utils", MessageBoxButton.OK, MessageBoxImage.Error);
#endif

            if (classVariables.bRollbarActive)
                RollbarLocator.RollbarInstance.AsBlockingLogger(TimeSpan.FromSeconds(1)).Error(ex);
        }

        public static string Read_Registry(string sKey)
        {
            RegistryKey oRegistryKey = Registry.CurrentUser.OpenSubKey(classVariables.sSubKey);
            if (oRegistryKey == null)
                return null;
            else
                return oRegistryKey.GetValue(sKey.ToUpper()).ToString();
        }

        public static void Write_Registry(string sKey, string sValue)
        {
            RegistryKey oRegistryKey = Registry.CurrentUser.CreateSubKey(classVariables.sSubKey);
            oRegistryKey.SetValue(sKey.ToUpper(), sValue);
        }
    }
}
