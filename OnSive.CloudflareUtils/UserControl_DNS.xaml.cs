﻿using CloudFlare.Client.Models;
using System;
using System.Windows;
using System.Windows.Controls;

namespace OnSive.CloudflareUtils
{
    /// <summary>
    /// Interaction logic for UserControl_DNS.xaml
    /// </summary>
    public partial class UserControl_DNS : ListBoxItem
    {
        bool bBlockEvent = false;

        public UserControl_DNS(DnsRecord oDnsRecord)
        {
            try
            {
                InitializeComponent();

                this.DataContext = oDnsRecord;
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        public void Select()
        {
            try
            {
                bBlockEvent = true;
                this.IsSelected = true;
                CheckBox_Selection.IsChecked = true;
                bBlockEvent = false;
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        public void UnSelect()
        {
            try
            {
                bBlockEvent = true;
                this.IsSelected = false;
                CheckBox_Selection.IsChecked = false;
                bBlockEvent = false;
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private void CheckBox_Selection_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (bBlockEvent) return;
                bBlockEvent = true;
                this.IsSelected = true;
                bBlockEvent = false;
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private void CheckBox_Selection_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (bBlockEvent) return;
                bBlockEvent = true;
                this.IsSelected = false;
                bBlockEvent = false;
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private void ListBoxItem_Selected(object sender, RoutedEventArgs e)
        {
            try
            {
                if (bBlockEvent) return;
                bBlockEvent = true;
                CheckBox_Selection.IsChecked = true;
                bBlockEvent = false;
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }

        private void ListBoxItem_Unselected(object sender, RoutedEventArgs e)
        {
            try
            {
                if (bBlockEvent) return;
                bBlockEvent = true;
                CheckBox_Selection.IsChecked = false;
                bBlockEvent = false;
            }
            catch (Exception ex)
            {
                classFunctions.OnError(ex);
            }
        }
    }
}
